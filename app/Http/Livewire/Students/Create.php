<?php

namespace App\Http\Livewire\Students;

use App\Models\Student;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    // Ini akan digunakan untuk mengupload gambar dan untuk preview gambar
    use WithFileUploads;

    public $name, $nis, $photo;

    public function render()
    {
        return view('livewire.students.create');
    }

    public function store()
    {
        // Validasi data
        $this->validate([
            'nis' => ['required', 'numeric'],
            'name' => ['required'],
        ]);

        // Verifica si el usuario subió una foto, usando la foto
        // Si no, entonces usa la foto predeterminada
        if ($this->photo) {
            $photo = $this->photo->store('avatar/upload');
        } else {
            $photo = 'avatar/' . strtolower(substr($this->name, 0, 1)) . '.png';
        }

        Student::create([
            'nis' => $this->nis,
            'name' => $this->name,
            'photo' => $photo,
        ]);

        $this->emit('studentAdded');
    }
}
